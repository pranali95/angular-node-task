module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define('Product', {
      productName: {
        type:DataTypes.STRING
      },
      categoryId: {
        type:DataTypes.INTEGER
      },
      price: {
        type:DataTypes.INTEGER
      }
    }, {
        freezeTableName: true,
        tableName: 'Product',
    });
    Product.associate = function(models) {
        Product.belongsTo(models.Category,{foreignKey: 'categoryId'})
    };
    return Product;
  };
  