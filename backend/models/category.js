module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define('Category', {
      categoryName: {
        type:DataTypes.STRING
      }
    }, {
        freezeTableName: true,
        tableName: 'Category',
    });
    Category.associate = function(models) {
        Category.hasMany(models.Product,{foreignKey: 'categoryId'})
    };
    return Category;
  };
  