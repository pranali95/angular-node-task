module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define('Role', {
      role: {
        type:DataTypes.STRING
      }
    }, {
        freezeTableName: true,
        tableName: 'Role',
    });
    Role.associate = function(models) {
        Role.hasMany(models.User,{foreignKey: 'roleId'})
    };
    return Role;
  };
  