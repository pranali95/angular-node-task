const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstName: {
      type:DataTypes.STRING
    },
    lastName: {
      type:DataTypes.STRING
    },
    email: {
      type:DataTypes.STRING
    },
    password: {
      type:DataTypes.STRING
    },
    roleId:{
      type:DataTypes.INTEGER
    },
    refreshToken:{
      type:DataTypes.STRING
    }
  }, {
      freezeTableName: true,
      tableName: 'User',
  });
  User.associate = function(models) {
    User.belongsTo(models.Role,{foreignKey: 'roleId'})
  };

  // This hook is always run before create.

  User.beforeCreate(function (user, options, cb) {
    if (user.password) {
        return new Promise((resolve, reject) => {
            bcrypt.genSalt(10, function (err, salt) {
                if (err) {
                    return err;
                }
                bcrypt.hash(user.password, salt, function (err, hash) {
                    if (err) {
                        return err;
                    }
                    user.password = hash;
                    return resolve(user, options);
                });
            });
        });
    }
  });

  // Instance method for comparing password.

  User.prototype.comparePassword = function (passw, cb) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(passw, this.password, function (err, isMatch) {
            if (err) {
                return err;
            }
            return resolve(isMatch)
        });
    });
  };

  User.prototype.toJSON = function () {
    var values = Object.assign({}, this.get());
    delete values.password;
    delete values.refreshToken;
    return values;
}

  return User;
};


