const jwt = require('jsonwebtoken');
const models = require('../models'); 


module.exports = async (req, res, next) => {
    try {
        if(req.headers.authorization){
            let token = await req.headers.authorization.split(' ')[1];
        const decode = await jwt.verify(token, "userRefreshToken");
        let checkToken=await models.User.findOne({where:{refreshToken:token}});
        if(checkToken){
            let userDetails = await models.User.findOne({ where: {  id: decode.id } });
                        if (userDetails) {
                            delete userDetails.dataValues.refreshToken;
                            delete userDetails.dataValues.password;
                            req.userData = userDetails.dataValues;
                            next();
                        } else {
                            res.status(401).send({ message: 'Token is not valid' });
                        }
        } else {
            res.status(401).send({ message: 'Token is not valid' });
        }
        } else {
            res.status(401).send({ message: 'Token is not valid' });
        }
    } catch (err) {
        res.status(401).send({ message: 'access token is expired' });
    }
};

