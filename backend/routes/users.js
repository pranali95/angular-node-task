var express = require('express');
var router = express.Router();
let users=require('../controller/users');

/* GET users listing. */
router.post('/', users.AddUser);
router.get('/', users.getUser);

module.exports = router;



/**
* @swagger
* /user:
*   post:
*     tags:
*       - Users
*     summary: api to Register user
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             firstName:
*               type: string
*             lastName:
*               type: string
*             email:
*               type: string
*             password:
*               type: string
*             roleId:
*               type: integer
*         required:
*           - firstName 
*           - lastName
*           - email
*           - password
*           - roleId
*     responses:
*       201:
*         description: User created
*       422:
*         description: User not created
*   get:
*     tags:
*       - Users
*     summary: To read all User 
*     security:
*       - bearerAuth: [] 
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: User found. 
*       404:    
*         description: The user does not exit.
*/