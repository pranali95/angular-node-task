var express = require('express');
var router = express.Router();
let users=require('./users');
let role=require('./role');
let auth=require('./auth');
let category=require('./category');
let product=require('./product');
/* GET home page. */
router.use('/auth',auth);
router.use('/user',users);
router.use('/role',role);
router.use('/category',category);
router.use('/product',product); 

module.exports = router;
