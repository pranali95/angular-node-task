var express = require('express');
var router = express.Router();
let role=require('../controller/role');

/* GET users listing. */
router.post('/', role.AddRole);
router.get('/', role.ReadRole);

module.exports = router;



/**
* @swagger
* /role:
*   post:
*     tags:
*       - Role
*     summary: api to add role
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             role:
*               type: string
*         required:
*           - role
*     responses:
*       201:
*         description: Role added
*       422:
*         description: Role not created
*   get:
*     tags:
*       - Role
*     summary: api to read role
*     consumes:
*       - application/json
*     responses:
*       200:
*         description: Role details
*       404:
*         description: no data found
*/