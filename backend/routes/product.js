var express = require('express');
var router = express.Router();
let product=require('../controller/product');
const checkAuth=require('../middleware/checkAuth');
const checkRole=require('../middleware/checkRolProduct');

/* GET users listing. */
router.post('/',checkAuth,checkRole,product.AddProduct);
router.get('/',checkAuth, product.ReadAllProduct);
router.put('/:id',checkAuth,checkRole, product.EditProduct);

module.exports = router;
