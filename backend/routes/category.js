var express = require('express');
var router = express.Router();
let category=require('../controller/category');
const checkAuth=require('../middleware/checkAuth');
const checkRole=require('../middleware/checkRoleCategory');

/* GET users listing. */
router.post('/',checkAuth,checkRole,category.AddCategory);
router.get('/',checkAuth, category.ReadAllCategory);
router.put('/:id',checkAuth,checkRole, category.EditCategory);
router.get('/:id',checkAuth, category.ReadOneCategory);

module.exports = router;
