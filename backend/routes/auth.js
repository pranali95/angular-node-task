var express = require('express');
var router = express.Router();
let auth=require('../controller/auth');
const checkAuth=require('../middleware/checkAuth');

/* GET users listing. */
router.post('/', auth.LogIn);
router.get('/logout',checkAuth, auth.LogOut);

module.exports = router;



/**
* @swagger
* /auth:
*   post:
*     tags:
*       - Authentication
*     summary: api to login
*     consumes:
*       - application/json
*     parameters:
*       - name: body
*         in: body
*         schema:
*           type: object
*           properties:
*             email:
*               type: string
*             password:
*               type: string
*         required:
*           - email
*           - password
*     responses:
*       200:
*         description: success
*       404:
*         description: user not found
* /auth/logout:
*   post:
*     tags:
*       - Authentication
*     name: Admin Logout
*     summary: For Admin Logout
*     security:
*       - bearerAuth: []
*     responses:
*       200:
*         description: Success
*/