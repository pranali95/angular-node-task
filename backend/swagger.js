const swaggerJSDoc = require("swagger-jsdoc");
const express = require("express");
const app = express();

const swaggerDefinition = {
    info: {
      swagger: "2.0",
      title: 'Node task api',
      description: 'APIs details',
    },
    host: 'localhost:3000',
    basePath: '/api',
    securityDefinitions: {
      bearerAuth: {
        type: 'apiKey',
        name: 'Authorization',
        scheme: 'jwt',
        in: 'header',
      },
    },
  };
  const options = {
    swaggerDefinition,
    apis: ['./routes/*.js'],
  };
  
  module.exports.swaggerSpec = swaggerJSDoc(options);
  app.get('/swagger.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

