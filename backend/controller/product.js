const models=require('../models');

exports.AddProduct=async(req,res)=>{
    try{
    let {productName,categoryId,price}=req.body;
    categoryId=parseInt(categoryId);
    price=parseInt(price);
    let createProduct=await models.Product.create({productName,categoryId,price});
    if(!createProduct){
        res.status(422).json({message:"product not created"})
    } else {
        res.status(201).json({message:"product craeted",createProduct});
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}

exports.EditProduct=async(req,res)=>{
    try{
    const Catid = req.params.id;
    let id=parseInt(Catid);
    let {productName,categoryId,price}=req.body;
    let editProduct=await models.Product.update({productName,categoryId,price},{where:{id}})
    if(editProduct[0] === 0){
        res.status(404).json({message:"data not found"})
    } else {
        res.status(200).json({message:"success"});
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}

exports.ReadAllProduct=async(req,res)=>{
    try{
    let productData=await models.Product.findAll(
       { include: [{
            model: models.Category
        }]
    }
    );
    if(productData.length==0){
        res.status(404).json({message:"data not found"})
    } else {
        res.status(200).json(productData);
    }
    }catch(err){
        console.log(err)
        res.status(500).json("somting went wrong");
    }
}
