const models=require('../models');

exports.AddUser=async(req,res)=>{
    try{
    let {firstName,lastName,email,password,roleId}=req.body;
    let RoleId=parseInt(roleId);
    let user=await models.User.create({firstName,lastName,email,password,roleId:RoleId});
    if(!user){
        res.status(422).json({message:"user not created"})
    } else {
        res.status(201).json({message:"user created",user});
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}

exports.getUser=async(req,res)=>{
    try{
        let users=await models.User.findAll();
        if(users.length==0){
            res.status(404).json({message:"data not found"});
        } else {
            res.status(200).json(users);
        }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}