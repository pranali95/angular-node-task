const models=require('../models');

exports.AddCategory=async(req,res)=>{
    try{
    let {categoryName}=req.body;
    let createCategory=await models.Category.create({categoryName});
    if(!createCategory){
        res.status(422).json({message:"category not created"})
    } else {
        res.status(201).json({message:"category craeted",createCategory});
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}

exports.EditCategory=async(req,res)=>{
    try{
    const Catid = req.params.id;
    let id=parseInt(Catid);
    let {categoryName}=req.body;
    let editCategory=await models.Category.update({categoryName},{where:{id}})
    if(editCategory[0] === 0){
        res.status(404).json({message:"data not found"})
    } else {
        res.status(200).json({message:"success"});
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}

exports.ReadAllCategory=async(req,res)=>{
    try{
    let categoryData=await models.Category.findAll();
    if(categoryData.length==0){
        res.status(404).json({message:"data not found"})
    } else {
        res.status(200).json(categoryData);
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}

exports.ReadOneCategory=async(req,res)=>{
    try{
    const Catid = req.params.id;
    let id=parseInt(Catid);
    let getCat=await models.Category.findOne({where:{id}});
    if(!getCat){
        res.status(404).json({message:"data not found"})
    } else {
        res.status(200).json(getCat);
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}