const models=require('../models');
const jwt = require('jsonwebtoken');

exports.LogIn=async(req,res)=>{
    try{
    const { email, password } = req.body;
    let checkUser = await models.User.findOne({ where: { email } });
    if (!checkUser) {
        res.status(404).json({ message: 'User not found' })
    } else {
        let userDetails = await checkUser.comparePassword(password);
        if (userDetails){
            const refreshToken = jwt.sign({
                id: checkUser.dataValues.id,
                mobile: checkUser.dataValues.email,
                userName: checkUser.dataValues.firstName
            },
            "userRefreshToken");
            let updateRefreshToken = await models.User.update({ refreshToken }, { where: { id: checkUser.dataValues.id} });
            if (updateRefreshToken) {
                res.status(200).json({ message: 'success', refreshToken,userData:checkUser });
            }
        } else {
            res.status(401).json({message:"You entered wrong password"})
        } 
    }
    }catch(err){
        console.log(err)
        res.status(500).json("somting went wrong");
    }
}



exports.LogOut=async(req,res)=>{
    try{
        let updateUser = await models.User.update({ refreshToken: null },
        { where: { id: req.userData.id } });
    if (updateUser) {
        res.status(200).json({ message: 'Success' });
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}