const models=require('../models');

exports.AddRole=async(req,res)=>{
    try{
    let {role}=req.body;
    let createRole=await models.Role.create({role});
    if(!createRole){
        res.status(422).json({message:"user not created"})
    } else {
        res.status(201).json({message:"role craeted",createRole});
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}

exports.ReadRole=async(req,res)=>{
    try{
    let getRole=await models.Role.findAll();
    if(getRole.length==0){
        res.status(404).json({message:"data not found"})
    } else {
        res.status(200).json(getRole);
    }
    }catch(err){
        res.status(500).json("somting went wrong");
    }
}