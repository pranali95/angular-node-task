import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { InterceptService } from '../_services/intercept.service';
import { HTTP_INTERCEPTORS,HttpClientModule } from '@angular/common/http';




const routes: Routes = [
  {
    path:'',component:LoginComponent
  },
  {
    path:'register',
    component:RegisterComponent
  }
];


@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  imports: [
    CommonModule,
    FormsModule,
		ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    InterceptService,
    HttpClientModule,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		}]
})
export class AuthModule { }
