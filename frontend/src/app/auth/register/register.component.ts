import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../_services/auth.service"
import {RegisterService} from "../../_services/register.service"

import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public register:FormGroup;
  public roleData:any;
  constructor(
     public fb: FormBuilder,
    public authService:AuthService,
    public route: Router,
    public registerService:RegisterService) { }

  ngOnInit() {
    this.getRole();
    this.regisData();
  }


  regisData() {
    this.register = this.fb.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      email: ["", Validators.required],
      password: ["", Validators.required],
      roleId: ["", Validators.required]
      
    });
  }

  submit(value){
    this.registerService.registerUser(value).subscribe(res=>{
      this.route.navigate(['/auth']);
    },err=>{
  alert(err['error']['message'])

    })
  }


getRole(){
this.registerService.getRole().subscribe(res=>{
this.roleData=res;
},
err=>{
  alert(err['error']['message'])
})
  
}
}
