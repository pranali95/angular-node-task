import { Component, OnInit } from '@angular/core';
import { InterceptService } from "../../_services/intercept.service";
import {AuthService} from "../../_services/auth.service"
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
dataaaa;
public loginData:any;

public login:FormGroup;
  constructor(public InteractionService:InterceptService,
     public fb: FormBuilder,
    public authService:AuthService,
    public route: Router) { }

  ngOnInit() {
    this.loginDataFun() 
  }


  loginDataFun() {
    this.login = this.fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
      
    });
  }

  submit(){
    let email: Number = this.login.controls['email'].value;
    let password: Number = this.login.controls['password'].value;

    this.authService.userlogin(email,password).subscribe(res =>{
      

      this.loginData = res;
      localStorage.setItem("token", this.loginData['refreshToken']);
      localStorage.setItem("userData", this.loginData['userData']['roleId']);
      this.route.navigate(['/home']);
    },err=>{
       alert(err['error']['message'])
    }
    )
  }
 
  register(){

    this.route.navigate(['/auth/register']);


  }

}
