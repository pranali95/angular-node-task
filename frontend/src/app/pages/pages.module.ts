import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './category/category.component';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { InterceptService } from '../_services/intercept.service';
import { HTTP_INTERCEPTORS,HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'product', component: ProductComponent
  },
  {
    path: 'category', component: CategoryComponent
  },
  {
    path: '**', component: HomeComponent
  },
];

@NgModule({
  declarations: [CategoryComponent, ProductComponent, HomeComponent, NavbarComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    FormBuilder,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		}]
})
export class PagesModule { }
