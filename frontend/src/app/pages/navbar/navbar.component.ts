import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../_services/auth.service";
import { Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  roleId;
  constructor(private router : Router,public auth:AuthService) { }

  ngOnInit() {
     this.roleId=localStorage.getItem('userData')
  }

  getRole(){
    if(this.roleId==1){
      return true
    } else {
      return false
    }
  }

  logout(){
    this.auth.logoutUser().subscribe(res=>{
      localStorage.clear()
      this.router.navigate(['/auth']);
    })
  }

}
