import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ProductService } from '../../_services/product.service';
import { CategoryService } from '../../_services/category.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  productForm: FormGroup;
  allProducts;
  selectedProduct: any;
  categories: any;
  selectedId: any;
  constructor(private fb: FormBuilder, private productService: ProductService, private categoryService: CategoryService) { }


  ngOnInit() {

    this.productForm = this.fb.group({
     categoryId: ['', [Validators.required]],
      productName: ['', [Validators.required]],
      price: ['', [Validators.required]],
      id:['']
    });

    this.getProduct();
    this.getCategory();
  }

  getCategory() {
    this.categoryService.getCategory().subscribe(res => {
      this.categories = res;
    },
      error => {
        console.error(error);
      });
  }

  getProduct() {
    this.productService.getProduct().subscribe(res => {
      console.log(res);
      this.allProducts = res;
    },
      error => {
        console.error(error);
      });
  }

  addProduct() {
    const data = this.productForm.value;

    this.productService.addProduct(data).subscribe(res=>{
      this.ngOnInit();
    });
  }

  getProductSingle(value) {
    this.productForm.patchValue(value);
  }

  updateProduct() {

    const data = this.productForm.value;
    this.productService.updateProduct(data).subscribe(res => {
      this.productForm.reset();
      alert("updated")
      this.ngOnInit();
    },
    err=>{
      alert(err['error']['message'])
    });
  }

}
