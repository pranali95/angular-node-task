import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from '../../_services/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categoryForm: FormGroup;
  allCategories: any;
  productDetails: any;
  selectedId: any;
  constructor(public route: Router, private fb: FormBuilder, private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryForm = this.fb.group({
      categoryName: ['', [Validators.required]],
      id:[]
    });

    this.getCategory();
  }

  getCategory() {
    this.categoryService.getCategory().subscribe(res => {
      console.log(res);
      this.allCategories = res;
    });
  }

  addCategory() {
    const data = this.categoryForm.value;

    this.categoryService.addCategory(data).subscribe(res => {
      console.log(res);
      this.categoryForm.reset();
      this.ngOnInit();
    },
      error => {
        console.error(error);
      });
  }

  getCategorySingle(value) {
    this.categoryForm.patchValue(value);
  }

  updateCategory() {

    const data = this.categoryForm.value;

    this.categoryService.updateCategory(data).subscribe(res => {
      this.ngOnInit();
    });
  }

}
