import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{ErrorComponent} from '../app/error/error.component';
import{AuthGuard} from '../app/_helpers/auth.guard';



const routes: Routes = [
  {
    path:'auth',loadChildren: ()=>import('../app/auth/auth.module').then(m=>m.AuthModule)
  },
  {
    path:'',loadChildren:()=>import('../app/pages/pages.module').then(m=>m.PagesModule),canActivate: [AuthGuard]
  },
  {
    path:'**',component : ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
