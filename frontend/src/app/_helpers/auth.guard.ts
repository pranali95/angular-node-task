import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../_services/auth.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(private router : Router, public auth:AuthService){}
  canActivate(){
    if(this.auth.getToken()){
      return true
    } else {
      this.router.navigate(['/auth']);
      return false
    }
  }
  
}
