// Angular
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';

import { AuthService} from './auth.service';

@Injectable()
export class InterceptService implements HttpInterceptor {
	constructor( 
		private injector: Injector
	  ) { }

	// intercept request and add token
	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	){
		let authService =this.injector.get(AuthService)
		let tokenizedReq = request.clone({
			setHeaders :{
			  Authorization : `Bearer ${authService.getToken()}`
			}
		  })
		
		  return next.handle(tokenizedReq)
		}
	}

