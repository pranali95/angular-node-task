import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategory(): Observable<any> {
    return this.http.get(`/api/category`);
  }

  getCategorySingle(id): Observable<any> {
    return this.http.get(`/api/category/${id}`);
  }

  addCategory(data): Observable<any> {
    return this.http.post(`/api/category`, data);
  }

  updateCategory(data): Observable<any> {
    return this.http.put(`/api/category/${data.id}`, data);
  }

  // deleteCategory(data): Observable<any> {
  //   return this.http.post(`/api/category`, data);
  // }
}
