import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getProduct(): Observable<any> {
    return this.http.get(`/api/product`);
  }

  getProductSingle(id): Observable<any> {
    return this.http.get(`/api/product/${id}`);
  }

  addProduct(data): Observable<any> {
    return this.http.post(`/api/product`, data);
  }

  updateProduct(data): Observable<any> {
    return this.http.put(`/api/product/${data.id}`, data);
  }
}
