import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { User } from '../_models/User.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private _router: Router) { }
  userlogin(email,password): Observable<User> {

    return this.http.post<User>(`/api/auth`, {email,password})
}
getToken() {
  return localStorage.getItem('token')
}

logoutUser(){
  return this.http.get<any>('/api/auth/logout')
}
}
